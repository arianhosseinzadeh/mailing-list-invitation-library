# Mailing List Invitation System (3rd party library) #

A JavaScript based system to present a DHTML invitation on a website to sign up for a mailing list. This system:

Triggers the invitation only after the user has been on the site for x seconds.

Triggers the invite after a certain number of pages have been visited.

Allows for easy customization of the invitation. 

## Pull & Run ##

git clone git@bitbucket.org:arianhosseinzadeh/foresee-int.git

npm install

gulp

Open index.html