var gulp = require('gulp'),
    es6Path = './src/*.js',
    browserify = require('browserify'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream'),
    less = require('gulp-less'),
    path = require('path');

gulp.task('less', function () {
    return gulp.src('./src/*.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest('./src/'));
});

gulp.task('build', function () {
    return browserify({entries: './src/script.js', extensions: ['.js'], debug: true})
        .transform(babelify)
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', ['build'], function () {
    gulp.watch(es6Path, ['build']);
});

gulp.task('default', ['watch']);