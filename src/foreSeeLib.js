export default function foreSeeLib(pagesToVisit = 3, timeToSpend = 60){// the default values
    if (!window.foreSeeLib){
        window.foreSeeLib = new myLib(pagesToVisit,timeToSpend);
    }
}

class myLib {
    constructor(pagesToVisit, timeToSpend) {
        this.domain_name = document.domain || '';
        document.getElementsByTagName('body')[0].onbeforeunload = this._onClosePage.bind(this);
        //window.onbeforeunload = this._onClosePage.bind(this);
        myLib._createElements(); // create the grey layer element
        if (!this._getItem('totalTime')) // total time spent on any pages of the current domain
            this._setItem('totalTime', 0);
        if (!this._getItem('countPagesVisited')) { // total number of pages visited of the current domain
            this._setItem('countPagesVisited', 0);
        }
        this._incrementOpenWindowsCount(); // incremenet the number of tabs open for this domain (used in _clearUp)
        this._incrementVisit(); // increment the number of page visits of the current domain
        this._signalTime(pagesToVisit, timeToSpend); // start counting each second
    }

    static _createElements() { //create the grey layer used when pop up is shown
        //grey layer
        let blackOutDiv = document.createElement('div');
        document.getElementsByTagName('body')[0].appendChild(blackOutDiv);
        blackOutDiv.setAttribute('id', 'blackout');
        //stylesheet
        let styleSheetLink = document.createElement('link');
        styleSheetLink.type = 'text/css';
        styleSheetLink.rel = 'stylesheet';
        styleSheetLink.href = './src/foreSee.css';
        document.getElementsByTagName('head')[0].appendChild(styleSheetLink);
        //iframe and div
        let foreSeeIFrameWrapper = document.createElement('div');
        document.getElementsByTagName('body')[0].appendChild(foreSeeIFrameWrapper);
        foreSeeIFrameWrapper.setAttribute('id', 'foreSeeIFrameWrapper');
        let foreSeeIFrame = document.createElement('iframe');
        foreSeeIFrameWrapper.appendChild(foreSeeIFrame);
        foreSeeIFrame.setAttribute('id', 'foreSeeIFrame');
        foreSeeIFrame.setAttribute('src', './src/foreSeePopUp.html');
    }

    _signalTime(pagesToVisit, timeToSpend) {
        this._incrementVisitTime();
        //if the total time spent across all the tabs of the current domain or the number of pages visited
        //exceeded the values passed to this class , and we have not showed the pop up box yet , show it !
        if ((this._getItem('totalTime') > timeToSpend || this._getItem('countPagesVisited') > pagesToVisit) && !this._getItem('proposed')) {
            this._setItem('proposed', 'true');
            this._showPopUp();
        }
        if (!this._getItem('proposed')) // we only want to show the pop up box once
            setTimeout(this._signalTime.call(this , pagesToVisit, timeToSpend), 1000);
    }

    _getItem(key) {
        return localStorage.getItem(this.__createKey(key));
    }

    _setItem(key, value) {
        localStorage.setItem(this.__createKey(key), value)
    }

    __createKey(key) { // we create a create according to the key passed and the local domain
        //we may have this script running across different domains
        return key + "" + this.domain_name;
    }

    _removeItem(key) {
        localStorage.removeItem(this.__createKey(key));
    }

    _incrementVisit() {
        let count = this._getItem('countPagesVisited');
        count++;
        this._setItem('countPagesVisited', count);
    }

    _incrementVisitTime() {
        let t = this._getItem('totalTime');
        t++;
        this._setItem('totalTime', t);
    }

    _incrementOpenWindowsCount() {
        let openWindowsCount = this._getItem('countOpenWindow') || 0;
        openWindowsCount++;
        this._setItem('countOpenWindow', openWindowsCount);
    }

    _onClosePage() { // call it once the page is closed or refreshed
        let openWindowsCount = this._getItem('countOpenWindow');
        openWindowsCount--;
        this._setItem('countOpenWindow' , openWindowsCount);
        if (openWindowsCount === 0) {
            this._clearUp();
        }
    }

    _clearUp() { // if there's no tab open , clear the keys we put in the local storage for the current domain
        this._removeItem('totalTime');
        this._removeItem('countPagesVisited');
        this._removeItem('proposed');
        this._removeItem('countOpenWindow');
    }

    saveEmail() { // get the value of the email and make an ajax call to the backend
        var iframe = document.getElementById('foreSeeIFrame');
        var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
        let emailAddress = innerDoc.getElementById('usersEmail').value;
        //make an ajax call and pass it to the backend
        this._hidePopUp();
    }

    _showPopUp() {
        document.getElementById('blackout').style.display = 'block';
        document.getElementById('foreSeeIFrameWrapper').style.display = 'table-cell';
    }

    _hidePopUp() {
        document.getElementById('blackout').style.display = 'none';
        document.getElementById('foreSeeIFrameWrapper').style.display = 'none';
    }
}

